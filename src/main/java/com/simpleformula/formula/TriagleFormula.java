package com.simpleformula.formula;

public class TriagleFormula {
    
    public static double calculateArea(double floor, double heigth) {
        return floor * heigth / 2;
    }

    public static double calculateRadiant(double hypotenuse1, double hypotenuse2, double base) {
        return (hypotenuse1 + hypotenuse2 + base) ;
    }

    public static double calculateVolume(double side, double height) {
        return (side * side * height) / 3;
    }
}


