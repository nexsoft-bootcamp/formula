package com.simpleformula.formula;

import java.util.Scanner;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormulaApplication {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Circle myCircle = new Circle(5.0);
        System.out.println("Radius of the circle: " + myCircle.getRadius());
        System.out.println("Area of the circle: " + myCircle.calculateArea());
        
        myCircle.setRadius(7.0);
        System.out.println("Updated radius: " + myCircle.getRadius());
        System.out.println("Updated area: " + myCircle.calculateArea());

		System.out.println("Perhitungan luas area segitiga");

		triangleMenu(input);
	}

	public static void triangleMenu(Scanner input) {
		int floor, height;
		double result, ans;

		System.out.println("==== Rumus Segitiga ====");
		System.out.print("Masukkan alas segitiga   : ");
		floor = input.nextInt();
		System.out.print("Masukkan tinggi segitiga : ");
		height = input.nextInt();

		result = TriagleFormula.calculateArea(floor, height);
		System.out.println("Hasil perhitungan : " + result);

        ans = TriagleFormula.calculateRadiant(floor, height, result);
        System.out.println("Keliling segitia : " + ans);
	}
}
